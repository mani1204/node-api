const mongoose = require("mongoose")

const connectToDB=async()=>{
    const DBcon= await mongoose.connect(process.env.db_con_url,
        {useNewUrlParser:true,useUnifiedTopology:true})
    
    console.log("MongoDB Connected Successfully with ",DBcon.connection.host)
}
module.exports=connectToDB