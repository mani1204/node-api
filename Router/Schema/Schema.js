const mongoose = require("mongoose")

const DataBaseSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    age:{
        type:Number,
        required:true,
        trim:true
    },
    place:{
        type:String,
        required:true,
        trim:true
    },
    edu:{
        type:String,
        required:true,
        trim:true
    },
    prof:{
        type:String,
        required:true,
        trim:true
    }
})

module.exports = mongoose.model("emp_detail",DataBaseSchema)