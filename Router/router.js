const express = require("express")
const DBSchema = require("./Schema/Schema")
const router = express.Router()

router.get("/",async(req,res,next)=>{
    let data = await DBSchema.find()
    res
    .status(201)
    .json({message:"Success",data:data})
})
router.post("/",async(req,res,next)=>{
    if("_id" in req.body.data){
        alert("yes")
    }
    else{
        let newData={
            name:req.body.data.name,
            place:req.body.data.place,
            age:req.body.data.age,
            edu:req.body.data.edu,
            prof:req.body.data.prof
        }
        await DBSchema.create(newData)
        let data=await DBSchema.find()
        res.status(201).json({message:"Success",data})
    }
})
router.delete("/:id",async(req,res,next)=>{
    console.log("dele",req.params.id)
    await DBSchema.findByIdAndDelete(req.params.id)
    let data=await DBSchema.find()
    res.status(201).json({message:"Success",data})
})
router.put("/:id",async(req,res,next)=>{
    await DBSchema.updateOne({_id:req.body.data._id},{$set:{
        name:req.body.data.name,
        age:req.body.data.age,
        place:req.body.data.place,
        edu:req.body.data.edu,
        prof:req.body.data.prof,
    }})
    let data=await DBSchema.find()
    console.log(req.body.data)
    res.status(201).json({message:"Success",data})
})
module.exports=router